import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Home from './pages/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Cards from './components/Cards';
import Footer from './components/Footer'

function App() {
    return ( 
      <>
        <Router>
        <Navbar />
        
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' component={Cards} />


{/* Custom external destination routes for React Projects Component */}

          <Route path='/linkedin' component={() => { 
     window.open('https://linkedin.com/in/rey-jesus-teves-62b393100', '_blank'); 
     window.open('/about','_self');
     return null;
}}/>

          <Route path='/project-1' component={() => { 
     window.open('https://rey_jesus_teves.gitlab.io/capstone-1/', '_blank'); 
     window.open('/about','_self');
     return null;
}}/>


          <Route path='/project-2' component={() => { 
     window.open('https://rey_jesus_teves.gitlab.io/coursespage/index.html', '_blank'); 
     window.open('/about','_self');
     return null;
}}/>


          <Route path='/project-3' component={() => { 
     window.open('https://rey_jesus_teves.gitlab.io/coursecard/index.html', '_blank'); 
     window.open('/about','_self');
     return null;
}}/>


          <Route path='/contactpage' component={() => { 
     window.open('https://rey_jesus_teves.gitlab.io/capstone-1/#contact', '_blank'); 
     window.open('/about','_self');
     return null;
}}/>

          <Route path='/email' component={() => { 
     window.open('mailto:reyjesusmirandateves@gmail.com?Subject=Good%20News%20Rey', '_blank'); 
     window.open('/about','_self');
     return null;
}}/>


        </Switch>
      </Router> 

      <Footer/>
      
      </>
    );
}




export default App;