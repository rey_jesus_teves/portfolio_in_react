import React from 'react'
import { Form, Button } from 'react-bootstrap'
import styles from './ContactForm.module.css'

export default function ContactForm() {

	return (
		<div className={styles.bg}>
		<section className={styles.section} id="contact">
			<h1 className={styles.sectionHeader}>Send Email</h1>
            
            <Form
            className={styles.contactForm}
            action="https://formspree.io/f/xayladjw"
			method="POST"
            >
                <Form.Group className={styles.formGroup}>
                    <Form.Label className={styles.formLabel}>Email:</Form.Label>
                    <Form.Control type="email" name="_replyto" />
                </Form.Group>
                <Form.Group className={styles.formGroup}>
                <Form.Label className={styles.formLabel}>Message:</Form.Label>
                    <Form.Control as="textarea" rows={3} name="message" />
                </Form.Group>
                <Button className={styles.formButton} block type="submit">Submit</Button>
            </Form>
			
			
		
		</section>
		</div>
	)
}
