import React from 'react';
import '../App.css';
import './HeroSection.css';

function HeroSection() {
  return (
    <div className='hero-container'>
      <h1 id="heading">REY JESUS TEVES</h1>
      <span id="subheading">WEB DESIGNER AND DEVELOPER</span>

    </div>
  );
}

export default HeroSection;
