import React from 'react';
import '../App.css';
import Cards from '../components/Cards';
import HeroSection from '../components/HeroSection';
import ContactForm from '../components/ContactForm'


function Home() {
  return (
    <>
      <HeroSection />
      <Cards />
      <ContactForm />
      
    </>
  );
}

export default Home;
